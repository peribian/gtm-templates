# Ecommerce -> GA4 Items variable template

Use this template to remap your third-party ecommerce objects to [GA4 items format](https://developers.google.com/analytics/devguides/collection/ga4/ecommerce?client_type=gtm#add_or_remove_an_item_from_a_shopping_cart).

[Detailed documentation](https://docs.google.com/document/d/1qB7a8S-lEWhZrRjjMkjSL8v42_N0stAqt0muoLChfao/edit?usp=sharing).