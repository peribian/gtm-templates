﻿___INFO___

{
  "type": "MACRO",
  "id": "cvt_temp_public_id",
  "version": 1,
  "securityGroups": [],
  "displayName": "ecommerce object -\u003e GA4 item",
  "description": "Map custom e-commerce object array to GA4 format.",
  "containerContexts": [
    "WEB"
  ]
}


___TEMPLATE_PARAMETERS___

[
  {
    "type": "TEXT",
    "name": "dl_variable_name",
    "simpleValueType": true,
    "defaultValue": "trackingData",
    "displayName": "Data Layer Ecommerce Items Variable Name"
  },
  {
    "type": "TEXT",
    "name": "fallback_value",
    "displayName": "Fallback value",
    "simpleValueType": true,
    "defaultValue": "not set"
  },
  {
    "type": "RADIO",
    "name": "output_format",
    "displayName": "Output Format",
    "radioItems": [
      {
        "value": "array",
        "displayValue": "array of objects"
      },
      {
        "value": "ga4",
        "displayValue": "GA4 object with items array"
      }
    ],
    "simpleValueType": true,
    "defaultValue": "ga4"
  },
  {
    "type": "GROUP",
    "name": "GA4 Item Prameter mapping",
    "displayName": "Parameter mapping",
    "subParams": [
      {
        "type": "TEXT",
        "name": "item_id",
        "displayName": "item_id",
        "simpleValueType": true,
        "defaultValue": ""
      },
      {
        "type": "TEXT",
        "name": "item_name",
        "displayName": "item_name",
        "simpleValueType": true,
        "defaultValue": ""
      },
      {
        "type": "TEXT",
        "name": "affiliation",
        "displayName": "affiliation",
        "simpleValueType": true,
        "defaultValue": ""
      },
      {
        "type": "TEXT",
        "name": "coupon",
        "displayName": "coupon",
        "simpleValueType": true,
        "defaultValue": ""
      },
      {
        "type": "TEXT",
        "name": "discount",
        "displayName": "discout",
        "simpleValueType": true,
        "defaultValue": ""
      },
      {
        "type": "TEXT",
        "name": "index",
        "displayName": "index",
        "simpleValueType": true,
        "defaultValue": ""
      },
      {
        "type": "TEXT",
        "name": "item_brand",
        "displayName": "item_brand",
        "simpleValueType": true,
        "defaultValue": ""
      },
      {
        "type": "TEXT",
        "name": "item_category",
        "displayName": "item_category",
        "simpleValueType": true,
        "defaultValue": ""
      },
      {
        "type": "TEXT",
        "name": "item_category2",
        "displayName": "item_category2",
        "simpleValueType": true,
        "defaultValue": ""
      },
      {
        "type": "TEXT",
        "name": "item_category3",
        "displayName": "item_category3",
        "simpleValueType": true,
        "defaultValue": ""
      },
      {
        "type": "TEXT",
        "name": "item_category4",
        "displayName": "item_category4",
        "simpleValueType": true,
        "defaultValue": ""
      },
      {
        "type": "TEXT",
        "name": "item_category5",
        "displayName": "item_category5",
        "simpleValueType": true,
        "defaultValue": ""
      },
      {
        "type": "TEXT",
        "name": "item_list_id",
        "displayName": "item_list_id",
        "simpleValueType": true,
        "defaultValue": ""
      },
      {
        "type": "TEXT",
        "name": "item_list_name",
        "displayName": "item_list_name",
        "simpleValueType": true,
        "defaultValue": ""
      },
      {
        "type": "TEXT",
        "name": "item_variant",
        "displayName": "item_variant",
        "simpleValueType": true,
        "defaultValue": ""
      },
      {
        "type": "TEXT",
        "name": "location_id",
        "displayName": "location_id",
        "simpleValueType": true,
        "defaultValue": ""
      },
      {
        "type": "TEXT",
        "name": "price",
        "displayName": "price",
        "simpleValueType": true,
        "defaultValue": ""
      },
      {
        "type": "TEXT",
        "name": "quantity",
        "displayName": "quantity",
        "simpleValueType": true,
        "defaultValue": ""
      }
    ],
    "groupStyle": "ZIPPY_CLOSED"
  }
]


___SANDBOXED_JS_FOR_WEB_TEMPLATE___

const log = require("logToConsole");
const copyFromDataLayer = require("copyFromDataLayer");
const Number = require("makeNumber");
const String = require("makeString");
const Object = require("Object");
const getType = require("getType");

// The keys of the GA4 add_to_cart event object properties and their corresponding types.
const GA4_ADD_TO_CART_EVENT_KEYS_BY_TYPE = {
  item_id: Number,
  item_name: String,
  affiliation: String,
  coupon: String,
  discount: Number,
  index: Number,
  item_brand: String,
  item_category: String,
  item_category2: String,
  item_category3: String,
  item_category4: String,
  item_category5: String,
  item_list_id: String,
  item_list_name: String,
  item_variant: String,
  location_id: String,
  price: Number,
  quantity: Number,
};

// Extract and validate the tracking data from the data layer.
let trackingData = copyFromDataLayer(data.dl_variable_name);
if (!trackingData) {
  log("missing trackingData");
}

// If trackingData is a single object, wrap it in an array
if (getType(trackingData) === "object") {
  trackingData = [trackingData];
}

// If there is no tracking data, fail gracefully
if (!trackingData || !trackingData.length) {
  log("trackingData should be in array format.");
  return [];
}

/**
 * Replace all occurrences of a specified value in a string with another value.
 * Function that replaces the String.prototype.replaceAll method.
 *
 * @param {string} txt - The original string where replacements will be made.
 * @param {string} oldValue - The value to be replaced.
 * @param {string} newValue - The new value to replace the old value.
 * @returns {string} A new string with all occurrences of the old value replaced by the new value.
 */
function replaceAll(txt, oldValue, newValue) {
  let replacedTxt = txt;
  while (replacedTxt.indexOf(oldValue) !== -1) {
    replacedTxt = replacedTxt.replace(oldValue, newValue);
  }
  return replacedTxt;
}

/**
 * Replace placeholders in a template string with corresponding values from a key-value map.
 *
 * @param {string} templateString - The template string containing placeholders.
 * @param {Object} keyValueMap - The key-value map where keys represent placeholders and values represent replacements.
 * @returns {string} A new string with all placeholders replaced by their corresponding values.
 */
function parseTemplateString(templateString, keyValueMap, fallbackValue) {
  // Inject values form the keyValueMap
  let parsedString = templateString;
  Object.keys(keyValueMap).forEach((key) => {
    const value = keyValueMap[key];
    parsedString = replaceAll(parsedString, "{" + key + "}", value);
  });

  // Inject fallbackValue for variables not found in the keyValueMap
  let variablePlaceholder = "";
  for (const char of templateString) {
    if (char === "{") {
      variablePlaceholder = "{";
    } else if (char === "}" && variablePlaceholder.length) {
      variablePlaceholder += "}";
      parsedString = parsedString.replace(variablePlaceholder, fallbackValue);
      variablePlaceholder = "";
    } else if (variablePlaceholder.length) {
      variablePlaceholder += char;
    }
  }

  return parsedString;
}

/**
 * Constructs a Google Analytics 4 add_to_cart event object from a custom tracking object.
 *
 * @param {Object} customTrackingObject - The object containing custom tracking properties.
 * @returns {Object} A Google Analytics 4 add_to_cart event object.
 */
function buildGa4Item(customTrackingObject) {
  const item = {};

  Object.keys(GA4_ADD_TO_CART_EVENT_KEYS_BY_TYPE).forEach((key) => {
    const propertyTemplateString = data[key];

    // If the input field isn't set set the property to undefined
    if (!propertyTemplateString) {
      item[key] = undefined;
      return;
    }

    const propertyValue = parseTemplateString(
      propertyTemplateString,
      customTrackingObject,
      data.fallback_value
    );
    const PropertyTypeClass = GA4_ADD_TO_CART_EVENT_KEYS_BY_TYPE[key];
    item[key] = PropertyTypeClass(propertyValue) || propertyValue;
  });

  return item;
}

const outputData = trackingData.map(buildGa4Item);

if (data.output_format === "ga4") {
  return { items: outputData };
}
return outputData;


___WEB_PERMISSIONS___

[
  {
    "instance": {
      "key": {
        "publicId": "logging",
        "versionId": "1"
      },
      "param": [
        {
          "key": "environments",
          "value": {
            "type": 1,
            "string": "debug"
          }
        }
      ]
    },
    "clientAnnotations": {
      "isEditedByUser": true
    },
    "isRequired": true
  },
  {
    "instance": {
      "key": {
        "publicId": "read_data_layer",
        "versionId": "1"
      },
      "param": [
        {
          "key": "allowedKeys",
          "value": {
            "type": 1,
            "string": "any"
          }
        }
      ]
    },
    "clientAnnotations": {
      "isEditedByUser": true
    },
    "isRequired": true
  }
]


___TESTS___

scenarios:
- name: Test remapping - array as input - ga4 as output
  code: |-
    const log = require('logToConsole');

    const mockData = {
      item_id: "{objectId}",
      item_name: "{objectName}",
      affiliation: "",
      coupon: "",
      discount: "",
      index: "",
      item_brand: "COLLECTION",
      item_category: "{objectType} - {objectNumberOfStars}",
      item_category2: "not set - not set",
      item_category3: "not set - not set",
      item_category4: "not set - not set",
      item_category5: "{objectRegion} - {objectDestination}",
      item_list_id: "",
      item_list_name: "",
      item_variant: "",
      location_id: "",
      price: "{price}",
      quantity: "{quantity}",
      output_format: "ga4",
    };

    const ecommerceObject = [
      {
        userId: "",
        topLevelObjectName: "Isabella Valamar Collection Island Resort 4*/5*",
        topLevelObjectId: 210,
        objectType: "Hotel",
        objectId: 18153,
        objectName: "Isabella Valamar Collection Island Resort Isabella Hotel",
        objectRegion: "Sjever",
        objectDestination: "Poreč",
        objectNumberOfStars: "4",
        pageType: "booking page",
        price: 284,
        priceInEuros: 284,
        currency: "EUR",
        quantity: 1,
        numberOfGuests: "A:2,C:0, P:0",
        numberOfNights: 1,
        bookingDate: "2024/05/27 - 2024/05/28",
        userGUID: "9335ddbb-0f46-420c-8955-9cc79d171f4d",
        customerID: "",
        loggedInState: "false",
      },
    ];

    const expectedOutput = {
      items: [
      {
        item_id: 18153,
        item_name: "Isabella Valamar Collection Island Resort Isabella Hotel",
        affiliation: undefined,
        coupon: undefined,
        discount: undefined,
        index: undefined,
        item_brand: "COLLECTION",
        item_category: "Hotel - 4",
        item_category2: "not set - not set",
        item_category3: "not set - not set",
        item_category4: "not set - not set",
        item_category5: "Sjever - Poreč",
        item_list_id: undefined,
        item_list_name: undefined,
        item_variant: undefined,
        location_id: undefined,
        price: 284,
        quantity: 1,
      },
    ]
    };

    mock("copyFromDataLayer", (key, dataLayerVersion) => ecommerceObject);

    // Call runCode to run the template's code.
    let variableResult = runCode(mockData);
    log("OUTPUT:");
    log(variableResult);

    // Verify that the variable returns a result.
    for (const i in variableResult) {
      assertThat(variableResult[i]).isEqualTo(expectedOutput[i]);
    }
- name: Test remapping - single object as input - array as output
  code: |-
    const log = require('logToConsole');

    const mockData = {
      item_id: "{objectId}",
      item_name: "{objectName}",
      affiliation: "",
      coupon: "",
      discount: "",
      index: "",
      item_brand: "COLLECTION",
      item_category: "{objectType} - {objectNumberOfStars}",
      item_category2: "not set - not set",
      item_category3: "not set - not set",
      item_category4: "not set - not set",
      item_category5: "{objectRegion} - {objectDestination}",
      item_list_id: "",
      item_list_name: "",
      item_variant: "",
      location_id: "",
      price: "{price}",
      quantity: "{quantity}",
    };

    const ecommerceObject = {
        userId: "",
        topLevelObjectName: "Isabella Valamar Collection Island Resort 4*/5*",
        topLevelObjectId: 210,
        objectType: "Hotel",
        objectId: 18153,
        objectName: "Isabella Valamar Collection Island Resort Isabella Hotel",
        objectRegion: "Sjever",
        objectDestination: "Poreč",
        objectNumberOfStars: "4",
        pageType: "booking page",
        price: 284,
        priceInEuros: 284,
        currency: "EUR",
        quantity: 1,
        numberOfGuests: "A:2,C:0, P:0",
        numberOfNights: 1,
        bookingDate: "2024/05/27 - 2024/05/28",
        userGUID: "9335ddbb-0f46-420c-8955-9cc79d171f4d",
        customerID: "",
        loggedInState: "false",
      };

    const expectedOutput = [
      {
        item_id: 18153,
        item_name: "Isabella Valamar Collection Island Resort Isabella Hotel",
        affiliation: undefined,
        coupon: undefined,
        discount: undefined,
        index: undefined,
        item_brand: "COLLECTION",
        item_category: "Hotel - 4",
        item_category2: "not set - not set",
        item_category3: "not set - not set",
        item_category4: "not set - not set",
        item_category5: "Sjever - Poreč",
        item_list_id: undefined,
        item_list_name: undefined,
        item_variant: undefined,
        location_id: undefined,
        price: 284,
        quantity: 1,
      },
    ];

    mock("copyFromDataLayer", (key, dataLayerVersion) => ecommerceObject);

    // Call runCode to run the template's code.
    let variableResult = runCode(mockData);
    log("OUTPUT:");
    log(variableResult);

    // Verify that the variable returns a result.
    for (const i in variableResult) {
      assertThat(variableResult[i]).isEqualTo(expectedOutput[i]);
    }
- name: Test fallback value
  code: "const log = require('logToConsole');\n\nconst mockData = {\n  fallback_value:\
    \ \"not set\",\n  \n  item_id: \"{objectId}\",\n  item_name: \"{objectName}\"\
    ,\n  affiliation: \"\",\n  coupon: \"\",\n  discount: \"\",\n  index: \"\",\n\
    \  item_brand: \"COLLECTION\",\n  item_category: \"{objectType} - {objectNumberOfStars}\"\
    ,\n  item_category2: \"not set - not set\",\n  item_category3: \"not set - not\
    \ set\",\n  item_category4: \"not set - not set\",\n  item_category5: \"{objectRegion}\
    \ - {objectDestination}\",\n  item_list_id: \"\",\n  item_list_name: \"\",\n \
    \ item_variant: \"\",\n  location_id: \"\",\n  price: \"{price}\",\n  quantity:\
    \ \"{quantity}\",\n};\n\nconst ecommerceObject = [\n  {\n    objectName: \"Valamar\
    \ Hotel\",\n  },\n];\n\nconst expectedOutput = [\n  {\n  item_id: \"not set\"\
    ,\n  item_name: \"Valamar Hotel\",\n  affiliation: undefined,\n  coupon: undefined,\n\
    \  discount: undefined,\n  index: undefined,\n  item_brand: \"COLLECTION\",\n\
    \  item_category: \"not set - not set\",\n  item_category2: \"not set - not set\"\
    ,\n  item_category3: \"not set - not set\",\n  item_category4: \"not set - not\
    \ set\",\n  item_category5: \"not set - not set\",\n  item_list_id: undefined,\n\
    \  item_list_name: undefined,\n  item_variant: undefined,\n  location_id: undefined,\n\
    \  price: \"not set\",\n  quantity: \"not set\",\n  },\n];\n\nmock(\"copyFromDataLayer\"\
    , (key, dataLayerVersion) => ecommerceObject);\n\n// Call runCode to run the template's\
    \ code.\nlet variableResult = runCode(mockData);\nlog(\"OUTPUT:\");\nlog(variableResult);\n\
    \n// Verify that the variable returns a result.\nfor (const i in variableResult)\
    \ {\n  assertThat(variableResult[i]).isEqualTo(expectedOutput[i]);\n}"


___NOTES___

Created on 2/13/2024, 3:37:06 PM


