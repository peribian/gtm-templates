# Custom Google Tag Manager Templates Repository

Welcome to our Custom Google Tag Manager Templates repository!
Here you'll find a collection of meticulously crafted templates designed to enhance your marketing efforts through Google Tag Manager (GTM).

## File Structure

The repository is organized into the following structure:

```
.
├── assets/
│   └── ... (image files)
├── README.md
├── tag-templates/
│   └── some-template/
│       ├── README.md
│       └── template.tpl    
└── variable-templates/
    └── some-template/
        ├── README.md
        └── template.tpl
```

- **assets/**: This directory contains images used in the README.md file.
- **tag-templates/**: This directory is intended for storing custom tag templates.
- **variable-templates/**: This directory is intended for storing variable templates.

Here's an improved version:

Each template should have its dedicated folder stored within either tag-templates/ or variable-templates. Each template folder contains:

 - template.tpl: This file contains the exported template from Google Tag Manager.
 
 - README.md: This README file documents the usage instructions and other relevant information for the template.


## Installation

1. **Download the Source Code**: Get the source code in `.zip` format from the [repository](https://gitlab.com/peribian/gtm-templates/-/tree/main?ref_type=heads).
   
   <img src="./assets/installation-1.png" alt="drawing" height="350"/>
   
2. **Unpack the ZIP File**: Extract the contents of the downloaded `.zip` document to a location on your local machine.
   
3. **Locate the Template File**: Navigate to the extracted files and find the `.tpl` file corresponding to the template you wish to install.

4. **Access Google Tag Manager**: Log in to your Google Tag Manager account and go to the workspace where you want to use the template.
   
5. **Navigate to Templates Section**: In the left-hand navigation menu, find and click on the "Templates" section.
   
6. **Create a New Template**: Click on the "New" button to create a new template.
   
7. **Import the Template File**: In the newly created template, click on the "Import" button and select the .tpl file from your local machine to import it into the template.

   <img src="./assets/installation-2.png" alt="drawing" height="350"/>

8. **Save the Template**: Now that you've imported the `.tpl` file, click on the "Save" button and you can start using your template.

## Development

### Creating a New Template

1. **Access Google Tag Manager**: Log in to your Google Tag Manager account and navigate to the workspace named `www.seekandhit.com` within the `ej-automation-testing` project.


2. **Navigate to Templates Section**: Locate the "Templates" section in the left-hand navigation menu and click on it.

1. **Create a New Template**: Within the "Templates" section, initiate the creation of a new template by clicking on the "New" button.

4. **Build the Template**: Develop the template by defining its user interface (UI), writing code, and setting up tests for your Google Tag Manager (GTM) template.

5. **Export the Template**: Once the template is complete, export its `.tpl` file. Ensure that all necessary documentation is included, and then add the exported file, along with the required documentation, to its corresponding directory within this repository.

3. **Clone the repository**:
   ```bash
   git clone git@gitlab.com:peribian/gtm-templates.git
   ```

4. **Locate the Template File**: Navigate to the cloned directory and find the `.tpl` file corresponding to the template you wish to install.

### Updating an Exising Template

1. **Clone the repository**:
   ```bash
   git clone git@gitlab.com:peribian/gtm-templates.git
   ```

2. **Locate the Template File**: Navigate to the extracted files and find the `.tpl` file corresponding to the template you wish to install.

3. **Access Google Tag Manager**: Log in to your Google Tag Manager account and go to the workspace where you want to use the template.

4. **Navigate to Templates Section**: In the left-hand navigation menu, find and click on the "Templates" section.

5. **Create a New Template**: Click on the "New" button to create a new template.
   
6. **Import the Template File**: In the newly created template, click on the "Import" button and select the `.tpl` file from your local machine to import it into the template.

   <img src="./assets/installation-2.png" alt="drawing" height="350"/>

7. **Make Changes**: Update the template where needed.

8. **Export the Template**: Once all modifications are complete, export the template as a `.tpl` file. Ensure that all essential documentation is included, and then add the exported file, along with any required documentation, to its designated directory within this repository.
